﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Controls all of the actions with a monobehaviour
/// </summary>
public class CinematicController : MonoBehaviour
{
    public UnityEvent OnActionFinished;

    [SerializeField]
    private CinematicActionControllerBase _actionController;

    /// <summary>
    /// Goes to the next Cinematic Action and activates it
    /// </summary>
    public void Progress()
    {
        if (GameController.GameState != GameState.Cinematic)
        {
            GameController.GameState = GameState.Cinematic;
            // TODO Util.GameController.CinematicPanel.Open();
        }
        _actionController.Progress();
    }
}