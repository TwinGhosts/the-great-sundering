﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Extensions for the Mathf class
/// </summary>
public static class Math
{
    public static float leftAngle, rightAngle;

    public static Vector2 VectorFromAngle(float dir)
    {
        return new Vector2(Mathf.Cos(Mathf.Deg2Rad * dir), Mathf.Sin(Mathf.Deg2Rad * dir));
    }

    public static float AngleFromVector(Vector2 dir)
    {
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }

    public static float AngleFromPointToMouse(Vector2 point)
    {
        var direction = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - point;
        return AngleFromVector(direction);
    }

    public static float Clamp0360(float angle)
    {
        var result = angle - Mathf.CeilToInt(angle / 360f) * 360f;

        if (result < 0)
        {
            result += 360f;
        }

        return result;
    }

    public static float ClampMinus180Positive180(float angle)
    {
        var result = angle - Mathf.CeilToInt(angle / 360f) * 360f;

        if (result < 0)
        {
            result += 360f;
        }

        return result - 180f;
    }

    /// <summary>
    /// Checks for all colliders in the Entity LayerMask and returns all of the Entities within the radius of an arc from a point
    /// </summary>
    /// <param name="start"></param>
    /// <param name="minAngle"></param>
    /// <param name="maxAngle"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    public static List<EntityBase> GetEntitiesInArc(Vector2 startPosition, float angle, float angularWidth, float radius)
    {
        var colliders = Physics2D.OverlapCircleAll(startPosition, radius, 1 << LayerMask.NameToLayer("Entity"));
        var targetList = new List<EntityBase>();

        var direction = VectorFromAngle(angle);
        var a = Clamp0360(AngleFromVector(direction));

        var left = Clamp0360(a - angularWidth / 2f);
        var right = Clamp0360(a + angularWidth / 2f);

        bool wrappedLeft = ((a - angularWidth / 2f) < 0f || (a - angularWidth / 2f) > 360f);
        bool wrappedRight = ((a + angularWidth / 2f) < 0f || (a + angularWidth / 2f) > 360f);
        bool wrappedOnce = (wrappedLeft || wrappedRight);

        foreach (var possibleTarget in colliders)
        {
            if (possibleTarget.GetComponent<EntityBase>() && possibleTarget.GetComponent<EntityBase>().Stats.Health > 0f)
            {
                var entityAngle = Clamp0360(AngleFromVector((Vector2)possibleTarget.transform.position - startPosition));

                if (wrappedOnce)
                {
                    if (wrappedLeft && entityAngle < right && entityAngle > left - 360f)
                    {
                        targetList.Add(possibleTarget.GetComponent<EntityBase>());
                    }
                    else if (wrappedLeft && entityAngle < right + 360f && entityAngle > left - 360f)
                    {
                        targetList.Add(possibleTarget.GetComponent<EntityBase>());
                    }
                    else if (wrappedRight && entityAngle > left && entityAngle < right + 360f)
                    {
                        targetList.Add(possibleTarget.GetComponent<EntityBase>());
                    }
                    else if (wrappedRight && entityAngle + 360f > left && entityAngle + 360f < right + 360f)
                    {
                        targetList.Add(possibleTarget.GetComponent<EntityBase>());
                    }
                }
                else
                {
                    if (entityAngle > left && entityAngle < right)
                    {
                        targetList.Add(possibleTarget.GetComponent<EntityBase>());
                    }
                }
            }
        }

        return targetList;
    }

    /// <summary>
    /// Checks for all colliders in the Entity LayerMask and returns all of the Entities within the radius of a circle around a point
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    public static List<EntityBase> GetEntitiesInCircle(Vector2 startPosition, float radius)
    {
        var colliders = Physics2D.OverlapCircleAll(startPosition, radius, LayerMask.NameToLayer("Entity"));
        var targetList = new List<EntityBase>();

        foreach (var possibleTarget in colliders)
        {
            if (possibleTarget.GetComponent<EntityBase>())
            {
                targetList.Add(possibleTarget.GetComponent<EntityBase>());
            }
        }

        return targetList;
    }
}
