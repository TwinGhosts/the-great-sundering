﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CinematicUtil
{
    /// <summary>
    /// Goes from Cinematic mode to Playing mode and disables the dialogueBox
    /// </summary>
    public static void ResetCinematicModeToPlaying()
    {
        GameController.GameState = GameState.Playing;
        Util.DialogueController.CloseDialogueBox();
        // TODO Util.CinematicPanel.Close();
    }
}
