﻿public enum AbilityState
{
    Enabled,
    Disabled,
}

public enum EffectTag
{
    Damage,
    DamageOverTime,

    Heal,
    HealOverTime,

    DecreaseMovementSpeed,
    DecreaseAttackSpeed,
    DecreaseCooldownSpeed,

    IncreaseAttackSpeed,
    IncreaseCooldownSpeed,
    IncreaseMovementSpeed,
}

public enum ArmorTag
{
    Light,
    Medium,
    Heavy,
    Magical,
    Divine,
}

public enum DamageTag
{
    Physical,
    Magical,
    Divine,
}

public enum GameState
{
    Playing,
    Paused,
    Cinematic,
}

public enum Direction
{
    Left,
    Up,
    Right,
    Down,
    None,
}

public enum ItemRarity
{
    Poor,
    Common,
    Uncommon,
    Rare,
    Epic,
    Legendary,
}

[System.Flags]
public enum UnitAttackType
{
    Physical = 1,
    Magic = 2,
    Divine = 4,
}

[System.Flags]
public enum UnitState
{
    None = 0,
    Idle = 1,
    Wandering = 2,
    Chasing = 4,
    Following = 8,
    Attacking = 16,
    Casting = 32,
}

public enum QuestObjectiveOrder
{
    Single,
    All,
}

public enum QuestType
{
    Story,
    Optional,
}

public enum Character
{
    Curios,
    Camlin,
    Agora,
    Kyron,
}