﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour
{
    public DebuffBase debuffToAdd;

    private void Start()
    {
        Util.QuestManager.SetQuestToActive(new Quest("Test Kill Quest", "Kill red 10 squares!", new QuestObjectiveKillEntity("Red Square", 10)));
    }

    // Update is called once per frame
    private void Update()
    {

    }
}
