﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class KeyHolder
{
    // Movement
    public static KeyCode moveLeft = KeyCode.A;
    public static KeyCode moveRight = KeyCode.D;
    public static KeyCode moveUp = KeyCode.W;
    public static KeyCode moveDown = KeyCode.S;

    public static KeyCode[] abilityKeys =
    {
        // Basic Attacks
        KeyCode.Mouse0,
        KeyCode.Mouse1,

        // Abilities
        KeyCode.Alpha1,
        KeyCode.Alpha2,
        KeyCode.Alpha3,
        KeyCode.Alpha4,
    };

    // Menu Screens
    public static KeyCode menuQuest = KeyCode.L;
    public static KeyCode menuInventory = KeyCode.I;
    public static KeyCode menuMap = KeyCode.M;
    public static KeyCode menuPause1 = KeyCode.P;
    public static KeyCode menuPause2 = KeyCode.Escape;

    // Interaction
    public static KeyCode interact = KeyCode.E;
}
