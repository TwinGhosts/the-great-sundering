﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CheckPointManager
{
    private static CheckpointWrapper _currentCheckPoint;

    public static CheckpointWrapper CurrentCheckPoint
    {
        get
        {
            return _currentCheckPoint;
        }
        set
        {
            _currentCheckPoint = value;
        }
    }
}

public class CheckpointWrapper
{
    public string Name = "Checkpoint: ";
    public Vector2 Position;

    public CheckpointWrapper(string name, Vector2 position)
    {
        Name += name;
        Position = position;
    }
}