﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Util
{
    public static EntityStats PlayerStats
    {
        get; set;
    }

    public static Transform WorldSpaceCanvas
    {
        get; set;
    }

    public static bool MenuSceneActive
    {
        get
        {
            return SceneManager.GetActiveScene().name == "Main Menu" || SceneManager.GetActiveScene().name == "Difficulty Select" || SceneManager.GetActiveScene().name == "Class Select" || SceneManager.GetActiveScene().name == "Credits";
        }
    }

    public static Player Player
    {
        get; set;
    }

    public static GameController GameController
    {
        get; set;
    }

    public static DungeonGenerator DungeonGenerator
    {
        get; set;
    }

    public static QuestManager QuestManager
    {
        get; set;
    }

    public static Inventory Inventory
    {
        get; set;
    }

    public static CinematicController CinematicController
    {
        get; set;
    }

    public static DialogueController DialogueController
    {
        get; set;
    }

    public static bool ExceedsBooleanThreshold(int threshold, IEnumerable<bool> booleans)
    {
        int trueCount = 0;
        foreach (bool boolean in booleans)
            if (boolean && (++trueCount > threshold))
                return true;
        return false;
    }
}