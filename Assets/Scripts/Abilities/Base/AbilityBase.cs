﻿using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityBase : MonoBehaviour
{
    [Header("Naming")]
    [SerializeField]
    protected string _name = "Generic Name";
    [SerializeField]
    [TextArea]
    protected string _description = "Generic Description";
    [SerializeField]
    [Space]
    protected Sprite _icon;

    [Space]
    [Header("Cooldown")]
    protected float _cooldown = 0f;
    [SerializeField]
    protected float _maxCooldown = 0f;
    protected float _referenceCooldown = 0f;

    [Space]
    [Header("Casting")]
    [SerializeField]
    protected float _castTime = 0f;

    [Space]
    [Header("State")]
    [SerializeField]
    protected AbilityState _state = AbilityState.Enabled;

    [Space]
    [Header("Debuffs")]
    [SerializeField]
    protected List<DebuffBase> _debuffs = new List<DebuffBase>();

    /// <summary>
    /// Starts casting an ability
    /// </summary>
    public virtual void Cast()
    {
        if (IsOnCooldown)
        {
            Debug.Log("Ability > Cooldown: " + Cooldown);
            return;
        }

        _cooldown = _maxCooldown;
    }

    protected virtual void Update()
    {
        if (IsOnCooldown)
        {
            _cooldown -= Time.deltaTime;
        }
    }

    /// <summary>
    /// Stops casting but regains the cooldown
    /// </summary>
    public virtual void CancelCast()
    {
        // TODO add cancel code
    }

    /// <summary>
    /// Stops casting and goes on cooldown
    /// </summary>
    public virtual void StopCast()
    {
        // TODO add stop code
    }

    public override string ToString()
    {
        return _name + "\n" + _description + "\n" + _cooldown;
    }

    #region Getters & Setters
    public AbilityState State
    {
        get
        {
            return _state;
        }
    }

    public string Name
    {
        get
        {
            return _name;
        }
    }

    public string Description
    {
        get
        {
            return _description;
        }
    }

    public float Cooldown
    {
        get
        {
            return _cooldown;
        }
    }

    public float MaxCooldown
    {
        get
        {
            return _maxCooldown;
        }
    }

    public List<DebuffBase> Debuffs
    {
        get
        {
            return _debuffs;
        }
    }

    public bool IsOnCooldown
    {
        get
        {
            return Cooldown > 0f;
        }
    }
    #endregion Getters & Setters
}
