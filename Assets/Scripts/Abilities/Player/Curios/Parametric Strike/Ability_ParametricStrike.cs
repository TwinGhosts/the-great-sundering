﻿using UnityEngine;

public class Ability_ParametricStrike : AbilityBase
{
    [Space]
    [Header("Custom Stats")]
    [SerializeField]
    private float[] _damage =
    {
        4f,
        4f,
        4f,
        6f,
    };

    [SerializeField]
    private float[] _knockbackPower =
    {
        2f,
        3f,
        3f,
        5f,
    };

    [SerializeField]
    private float[] _range =
    {
        2f,
        2.1f,
        2.2f,
        2.3f,
    };

    [SerializeField]
    private float[] _spread =
    {
        85f,
        90f,
        95f,
        105f,
    };

    [SerializeField]
    private float[] _stunDuration =
{
        1f,
        1f,
        1f,
        1.2f,
    };

    private int _currentCombo = 0;
    private const int MAX_COMBO = 4 - 1;

    private float _comboTimer = 0.45f;
    private float COMBO_TIMER = 0.45f;

    protected override void Update ()
    {
        base.Update();

        if (_currentCombo > 0 && _comboTimer > 0f && !IsOnCooldown)
        {
            _comboTimer -= Time.deltaTime;
        }
        else if (_currentCombo > 0 && _comboTimer <= 0f && !IsOnCooldown)
        {
            _cooldown = _maxCooldown / 1.8f;
            _currentCombo = 0;
        }
    }

    public override void Cast ()
    {
        base.Cast();

        // Get all entities in arc from the player towards the mouse and deal damage to the and apply the armor reduction debuff and knockback
        foreach (var target in Math.GetEntitiesInArc(transform.position, Math.AngleFromPointToMouse(transform.position), _spread[_currentCombo], _range[_currentCombo]))
        {
            target.GetComponent<EntityController>().Knockback(transform.position, _knockbackPower[_currentCombo]);
            target.AddDebuff(new Debuff_ArmorReduction(target, 5f, 2f, 0, UnitAttackType.Physical));
            target.AddDebuff(new Debuff_StunDamage(target, _damage[_currentCombo], _stunDuration[_currentCombo], 1, UnitAttackType.Physical));
        }

        // Check whether the currentCombo is lower than the maxCombo
        if (_currentCombo < MAX_COMBO)
        {
            _cooldown = 0f;
            _currentCombo++;
            _comboTimer = COMBO_TIMER;
        }
        else
        {
            _currentCombo = 0;
            _comboTimer = 0;
            _cooldown = _maxCooldown;
        }
    }

    private void OnDrawGizmos ()
    {
        var start = transform.position;
        var angle = Math.AngleFromPointToMouse(transform.position);
        var left = angle - _spread[_currentCombo] / 2f;
        var right = angle + _spread[_currentCombo] / 2f;
        var radius = _range[_currentCombo];

        Gizmos.color = (IsOnCooldown) ? Color.red : Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + ((Vector3)Math.VectorFromAngle(left)).normalized * radius);
        Gizmos.DrawLine(transform.position, transform.position + ((Vector3)Math.VectorFromAngle(right)).normalized * radius);

        Gizmos.color = (IsOnCooldown) ? Color.red : CUtil.ColorHolder.purple;
        Gizmos.DrawLine(transform.position, transform.position + ((Vector3)Math.VectorFromAngle(angle)).normalized * radius);

        Gizmos.color = (IsOnCooldown) ? Color.red : CUtil.ColorHolder.cyan;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
