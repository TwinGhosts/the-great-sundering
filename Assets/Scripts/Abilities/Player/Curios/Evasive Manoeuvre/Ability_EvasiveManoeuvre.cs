﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_EvasiveManoeuvre : AbilityBase
{
    [SerializeField]
    private float _duration = 1f;
    [SerializeField]
    private float _leapForce = 12f;
    [SerializeField]
    private float _hitRadius = 1f;
    [SerializeField]
    private float _hostileKnockback = 10f;
    [SerializeField]
    private float _hostileDamage = 3f;

    private float _timer = 0f;

    private bool _active = false;
    private List<EntityBase> _hostilesHit = new List<EntityBase>();

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (_active)
        {
            _timer += Time.deltaTime;
            var possibleHostiles = Physics2D.OverlapCircleAll(transform.position, _hitRadius);
            foreach (var possibleHostile in possibleHostiles)
            {
                // TODO fix for teams
                if (!_hostilesHit.Contains(possibleHostile.GetComponent<EnemyBase>()) && possibleHostile.GetComponent<EnemyBase>() && possibleHostile.GetComponent<EnemyBase>().Stats.Health > 0f)
                {
                    _hostilesHit.Add(possibleHostile.GetComponent<EnemyBase>());
                    possibleHostile.GetComponent<EntityController>().Knockback(transform.position, _hostileKnockback);
                    possibleHostile.GetComponent<EnemyBase>().AddDebuff(new Debuff_StunDamage(possibleHostile.GetComponent<EnemyBase>(), _hostileDamage, _duration, 1, UnitAttackType.Physical));
                }
            }
        }

        if (_timer >= _duration)
        {
            _active = false;
            _timer = 0f;
            _hostilesHit.Clear();
        }
    }

    public override void Cast()
    {
        base.Cast();

        var entity = GetComponentInParent<EntityBase>();
        _active = true;
        entity.AddDebuff(new Debuff_Invunerability(entity, _duration, 100f));
        entity.AddDebuff(new Debuff_StunDamage(entity, 0f, _duration, 1, UnitAttackType.Divine));
        entity.GetComponent<EntityController>().Knockback(Camera.main.ScreenToWorldPoint(Input.mousePosition), -_leapForce);
    }
}
