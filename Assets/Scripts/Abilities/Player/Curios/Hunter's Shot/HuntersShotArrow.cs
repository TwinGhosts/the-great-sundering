﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HuntersShotArrow : MonoBehaviour
{
    private float _damage = 0f;
    private float _speed = 1f;
    private int _maxTargetsHit = 1;
    private int _currentTargetsHit = 0;
    private Vector2 _direction;
    private List<EntityBase> _targetsHit = new List<EntityBase>();

    public void SetStats(float damage, float speed, int targets, Vector2 direction)
    {
        _damage = damage;
        _speed = speed;
        _maxTargetsHit = targets;
        _direction = direction;
    }

    // Update is called once per frame
    private void Update()
    {
        transform.position += (Vector3)_direction.normalized * Time.deltaTime * _speed;
        var possibleTargets = Physics2D.OverlapCircleAll(transform.position, 0.2f);
        foreach (var possibleTarget in possibleTargets)
        {
            if (possibleTarget.GetComponent<EnemyBase>() && !_targetsHit.Contains(possibleTarget.GetComponent<EnemyBase>()) && possibleTarget.GetComponent<EnemyBase>().Stats.Health > 0)
            {
                possibleTarget.GetComponent<EnemyBase>().Stats.Damage(_damage, UnitAttackType.Physical);
                _targetsHit.Add(possibleTarget.GetComponent<EnemyBase>());
                _currentTargetsHit++;
                if (_currentTargetsHit >= _maxTargetsHit)
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}
