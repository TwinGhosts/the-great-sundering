﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_HuntersShot : AbilityBase
{
    [SerializeField]
    private float _damage = 5f;
    [SerializeField]
    private float _speed = 2f;
    [SerializeField]
    private int _targets = 3;

    [SerializeField]
    private HuntersShotArrow _projectilePrefab;

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void Cast()
    {
        base.Cast();
        var projectile = Instantiate(_projectilePrefab, transform.position, Quaternion.identity);
        projectile.SetStats(_damage, _speed, _targets, Math.VectorFromAngle(Math.AngleFromPointToMouse(transform.position)));
    }
}
