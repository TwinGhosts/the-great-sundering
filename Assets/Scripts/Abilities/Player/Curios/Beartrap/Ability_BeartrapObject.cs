﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_BeartrapObject : MonoBehaviour
{
    private float _radius = 1f;
    private float _damage = 0f;
    private float _rootDuration = 0f;
    private float _lifeTime = 10f;
    private bool _triggered = false;
    private float _timer = 0f;

    private Debuff_RootDamage _debuff;

    public float Radius
    {
        get
        {
            return _radius;
        }

        set
        {
            _radius = value;
        }
    }

    public void SetStats(float radius, float damage, float rootDuration, float lifeTime)
    {
        _radius = radius;
        _damage = damage;
        _rootDuration = rootDuration;
        _lifeTime = lifeTime;
    }

    private void Activate(EntityBase target)
    {
        if (target)
        {
            target.AddDebuff(new Debuff_RootDamage(target, _damage, _rootDuration, 1, UnitAttackType.Physical));
            Destroy(gameObject);
        }
        else
        {
            _triggered = false;
        }
    }

    private void Update()
    {
        _timer += Time.deltaTime;
        if (_timer >= _lifeTime)
        {
            Destroy(gameObject);
        }

        if (Time.frameCount % 5 == 0 && !_triggered)
        {
            var entities = Physics2D.OverlapCircleAll(transform.position, _radius);
            var closestDistance = Mathf.Infinity;
            EnemyBase closestEnemy = null;

            foreach (var e in entities)
            {
                if (e.GetComponent<EnemyBase>() && (e.transform.position - transform.position).magnitude < closestDistance)
                {
                    closestEnemy = e.GetComponent<EnemyBase>();
                }
            }

            if (closestEnemy != null)
            {
                _triggered = true;
                Activate(closestEnemy);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, Radius);
    }
}
