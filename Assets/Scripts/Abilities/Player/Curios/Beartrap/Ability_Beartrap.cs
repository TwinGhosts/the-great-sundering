﻿using UnityEngine;
using UnityEngine.Serialization;

public class Ability_Beartrap : AbilityBase
{
    [Space]
    [Header("Custom Stats")]
    [SerializeField]
    private Ability_BeartrapObject _beartrap;

    [SerializeField]
    private float _damage = 10f;
    [SerializeField]
    private float _radius = 0.5f;
    [SerializeField]
    private float _rootDuration = 2f;
    [SerializeField]
    private float _lifeTime = 10f;

    protected override void Update()
    {
        base.Update();
    }

    public override void Cast()
    {
        base.Cast();

        var trap = Instantiate(_beartrap, transform.position, Quaternion.identity);
        trap.SetStats(_radius, _damage, _rootDuration, _lifeTime);

        _cooldown = _maxCooldown;
    }
}
