﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_Vanish : AbilityBase
{
    [SerializeField]
    private float _duration = 3f;
    [SerializeField]
    private float _speedUp = 50f;

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void Cast()
    {
        base.Cast();

        var entity = GetComponentInParent<EntityBase>();
        entity.AddDebuff(new Debuff_InvisibilityMovementSpeed(entity, _duration, _speedUp));
    }
}
