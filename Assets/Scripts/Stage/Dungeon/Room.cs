﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Holds the information of a Room
/// </summary>
public class Room
{
    public List<Vector2Int> Tiles;
    public List<Vector2Int> Walls = new List<Vector2Int>();
    public List<RoomDirectionPair> Neighbours = new List<RoomDirectionPair>();

    public class RoomDirectionPair
    {
        public Room room;
        public Direction direction;

        public RoomDirectionPair(Room room, Direction direction)
        {
            this.room = room;
            this.direction = direction;
        }
    }

    public Vector2Int horizontalBounds, verticalBounds;

    public List<Vector2Int> WallsLeft = new List<Vector2Int>();
    public List<Vector2Int> WallsRight = new List<Vector2Int>();
    public List<Vector2Int> WallsTop = new List<Vector2Int>();
    public List<Vector2Int> WallsBottom = new List<Vector2Int>();

    public Vector2Int startPosition;

    public Room(Vector2Int startPosition, Vector2Int horizontalBounds, Vector2Int verticalBounds, Direction direction)
    {
        this.startPosition = startPosition;
        this.horizontalBounds = horizontalBounds;
        this.verticalBounds = verticalBounds;
    }

    public Room(Vector2Int startPosition, Vector2Int horizontalBounds, Vector2Int verticalBounds)
    {
        this.startPosition = startPosition;
        this.horizontalBounds = horizontalBounds;
        this.verticalBounds = verticalBounds;
    }

    public RoomDirectionPair AddNeighbour(Room room, Direction direction)
    {
        var pair = new RoomDirectionPair(room, direction);

        if (!Neighbours.Contains(pair))
        {
            Neighbours.Add(pair);
        }
        else
        {
            pair = null;
        }

        return pair;
    }

    public void SetTiles(List<Vector2Int> tiles)
    {
        Tiles = tiles;
    }

    public void SetWallTile(int x, int y)
    {
        var wallPosition = new Vector2Int(x, y);
        Walls.Add(wallPosition);

        if (wallPosition.x == horizontalBounds.x)
        {
            WallsLeft.Add(wallPosition);
        }

        if (wallPosition.x == horizontalBounds.y - 1)
        {
            WallsRight.Add(wallPosition);
        }

        if (wallPosition.y == verticalBounds.x - 1)
        {
            WallsTop.Add(wallPosition);
        }

        if (wallPosition.y == verticalBounds.y)
        {
            WallsBottom.Add(wallPosition);
        }

        Util.DungeonGenerator.DungeonGrid[x, y] = 2;
    }
}
