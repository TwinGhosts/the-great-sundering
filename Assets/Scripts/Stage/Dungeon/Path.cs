﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Holds the information of a Path
/// </summary>
public class Path
{
    public List<Vector2Int> Tiles = new List<Vector2Int>();
    public List<Vector2Int> Walls = new List<Vector2Int>();
    public Vector2Int PathStart, PathEnd;

    public Path(Room startingRoom, Direction direction, int length)
    {
        Vector2Int startPos = Vector2Int.zero;
        switch (direction)
        {
            case Direction.Left:
                startPos = startingRoom.WallsLeft[Random.Range(1, startingRoom.WallsLeft.Count - 2)];
                break;
            case Direction.Up:
                startPos = startingRoom.WallsTop[Random.Range(1, startingRoom.WallsTop.Count - 2)];
                break;
            case Direction.Right:
                startPos = startingRoom.WallsRight[Random.Range(1, startingRoom.WallsRight.Count - 2)];
                break;
            case Direction.Down:
                startPos = startingRoom.WallsBottom[Random.Range(1, startingRoom.WallsBottom.Count - 2)];
                break;
        }

        for (int i = 0; i < length; i++)
        {
            var tilePos = new Vector2Int();
            switch (direction)
            {
                case Direction.Left:
                    tilePos = new Vector2Int(startPos.x - i, startPos.y);
                    Tiles.Add(tilePos);
                    Walls.Add(new Vector2Int(startPos.x - i, startPos.y + 1));
                    Walls.Add(new Vector2Int(startPos.x - i, startPos.y - 1));
                    break;
                case Direction.Up:
                    tilePos = new Vector2Int(startPos.x, startPos.y + i);
                    Tiles.Add(tilePos);
                    Walls.Add(new Vector2Int(startPos.x + 1, startPos.y + i));
                    Walls.Add(new Vector2Int(startPos.x - 1, startPos.y + i));
                    break;
                case Direction.Right:
                    tilePos = new Vector2Int(startPos.x + i, startPos.y);
                    Tiles.Add(tilePos);
                    Walls.Add(new Vector2Int(startPos.x + i, startPos.y + 1));
                    Walls.Add(new Vector2Int(startPos.x + i, startPos.y - 1));
                    break;
                case Direction.Down:
                    tilePos = new Vector2Int(startPos.x, startPos.y - i);
                    Tiles.Add(tilePos);
                    Walls.Add(new Vector2Int(startPos.x + 1, startPos.y - i));
                    Walls.Add(new Vector2Int(startPos.x - 1, startPos.y - i));
                    break;
            }

            if (i == length - 1)
            {
                PathEnd = tilePos;
            }
            PathStart = startPos;
        }

        // TODO Optimize
        foreach (var wall in Walls)
        {
            Util.DungeonGenerator.DungeonGrid[wall.x, wall.y] = 2;
        }

        foreach (var tile in Tiles)
        {
            Util.DungeonGenerator.DungeonGrid[tile.x, tile.y] = 1;
        }
    }
}
