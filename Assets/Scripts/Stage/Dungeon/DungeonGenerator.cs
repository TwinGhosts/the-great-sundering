﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DungeonGenerator : MonoBehaviour
{
    [Header("Rooms")]
    [SerializeField]
    private int _amountOfRooms = 10;
    [SerializeField]
    private int _minDistanceBetweenRooms = 5;
    [SerializeField]
    private int _maxDistanceBetweenRooms = 15;

    [SerializeField]
    private int _roomWidthMin = 3;
    [SerializeField]
    private int _roomWidthMax = 8;
    [SerializeField]
    private int _roomHeightMin = 3;
    [SerializeField]
    private int _roomHeightMax = 8;

    [Header("Tiles")]
    [SerializeField]
    private TileBase _groundTile;
    [SerializeField]
    private TileBase _wallTile;
    [SerializeField]
    private TileBase _roomWallTile;

    private Tilemap _dungeonMap;
    private int[,] _dungeonGrid;

    private const int DUNGEON_SIZE_MAX = 400;
    private const int DUNGEON_SIZE_HALF = 200;

    private int _numberOfRooms = 0;

    private List<Room> _rooms = new List<Room>();
    private List<Path> _paths = new List<Path>();

    public int[,] DungeonGrid
    {
        get
        {
            return _dungeonGrid;
        }

        set
        {
            _dungeonGrid = value;
        }
    }

    // Use this for initialization
    private void Awake()
    {
        Util.DungeonGenerator = this;

        // Define and get components
        DungeonGrid = new int[DUNGEON_SIZE_MAX, DUNGEON_SIZE_MAX];
        _dungeonMap = GetComponent<Tilemap>();

        // Generate a single room in the middle
        GenerateDungeon(new Vector2Int(DUNGEON_SIZE_HALF, DUNGEON_SIZE_HALF));

        // Set the camera
        Camera.main.transform.position = new Vector3(DUNGEON_SIZE_HALF, DUNGEON_SIZE_HALF, -10f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            GenerateDungeon(new Vector2Int(DUNGEON_SIZE_HALF, DUNGEON_SIZE_HALF));
        }
    }

    /// <summary>
    /// Generates the complete dungeon
    /// </summary>
    /// <param name="startingPoint"></param>
    private void GenerateDungeon(Vector2Int startingPoint)
    {
        // Clear the dungeon to start from a clean slate
        ResetDungeon();

        // Create the dungeon through iterations
        LayoutIterator(startingPoint);

        // Transfer the tile data to the Tilemap
        DrawDungeon();

        Debug.Log("> Finished the generation");
    }

    /// <summary>
    /// Loops through all Rooms and tries to create paths to new rooms
    /// </summary>
    /// <param name="startingPoint"></param>
    private void LayoutIterator(Vector2Int startingPoint)
    {
        // Init the first room as a reference point
        Room startingRoom = GenerateRoom(startingPoint);

        // Create the dungeon through iteration
        Debug.ClearDeveloperConsole();
        LayoutIteration(startingRoom);
    }

    /// <summary>
    /// A single room iteration through the dungeon generation
    /// </summary>
    /// <param name="creationPosition"></param>
    private void LayoutIteration(Room room)
    {
        // Calculate the maximum number of branches possible
        var maxBranches = (_amountOfRooms - _numberOfRooms) > 4 ? 4 : _amountOfRooms - _numberOfRooms;
        var amountOfRoomBranches = Random.Range(0, maxBranches);
        Room generatedRoom = null;

        var directionList = new List<Direction>() { Direction.Left, Direction.Right, Direction.Up, Direction.Down };
        for (int i = 0; i < amountOfRoomBranches; i++)
        {
            var direction = directionList[Random.Range(0, directionList.Count - 1)];
            var path = CreatePath(room, direction);
            directionList.Remove(direction);
            generatedRoom = GenerateRoom(path.PathEnd);

            if (_numberOfRooms < _amountOfRooms)
            {
                Debug.Log("+Room: " + _numberOfRooms);
                LayoutIteration(generatedRoom);
            }
        }

        // Break out of the iteration process when the number of rooms has been reached
        if (_amountOfRooms >= _numberOfRooms)
        {
            return;
        }
    }

    /// <summary>
    /// Resets the map to the initial state
    /// </summary>
    private void ResetDungeon()
    {
        // Set the whole map to walls
        for (var x = 0; x < DUNGEON_SIZE_MAX; x++)
        {
            for (var y = 0; y < DUNGEON_SIZE_MAX; y++)
            {
                DungeonGrid[x, y] = 0;
            }
        }

        // Reset the lists
        _rooms.Clear();
        _paths.Clear();
        _numberOfRooms = 0;
    }

    /// <summary>
    /// Generates a single room and marks the wall and innerTiles
    /// </summary>
    /// <param name="startingPosX"></param>
    /// <param name="startingPosY"></param>
    private Room GenerateRoom(Vector2Int startingPoint, Direction direction = Direction.None)
    {
        var roomWidth = Random.Range(_roomWidthMin, _roomWidthMax);
        var roomHeight = Random.Range(_roomHeightMin, _roomHeightMax);
        var roomGrid = new List<Vector2Int>();
        var room = new Room(new Vector2Int(startingPoint.x, startingPoint.y), new Vector2Int(startingPoint.x - roomWidth, startingPoint.x + roomWidth), new Vector2Int(startingPoint.y + roomHeight, startingPoint.y - roomHeight));

        // Loop through the roomSize on the grid
        for (var x = -roomWidth + startingPoint.x; x < roomWidth + startingPoint.x; x++)
        {
            for (var y = -roomHeight + startingPoint.y; y < roomHeight + startingPoint.y; y++)
            {
                // Add every tile to the Room
                roomGrid.Add(new Vector2Int(x, y));

                // Check for wall tiles, otherwise mark it as floor
                if (x == -roomWidth + startingPoint.x || x == roomWidth + startingPoint.x - 1 || y == -roomHeight + startingPoint.y || y == roomHeight + startingPoint.y - 1)
                {
                    room.SetWallTile(x, y);
                }
                else
                {
                    DungeonGrid[x, y] = 1;
                }
            }
        }

        // Add the grid to the room
        room.SetTiles(roomGrid);

        // Store the room
        _rooms.Add(room);
        _numberOfRooms++;

        return room;
    }

    /// <summary>
    /// Creates a path from a room towards a new direction
    /// </summary>
    /// <param name="fromRoom"></param>
    /// <returns></returns>
    private Path CreatePath(Room fromRoom, Direction direction)
    {
        var path = new Path(fromRoom, direction, Random.Range(Util.DungeonGenerator._minDistanceBetweenRooms, Util.DungeonGenerator._maxDistanceBetweenRooms));
        return path;
    }

    /// <summary>
    /// Sets the tiles in the Tilemap to the relative _dungeonGrid data
    /// </summary>
    private void DrawDungeon()
    {
        foreach (var path in _paths)
        {
            _dungeonGrid[path.PathStart.x, path.PathStart.y] = 1;
            _dungeonGrid[path.PathEnd.x, path.PathEnd.y] = 1;
        }

        for (var x = 0; x < DungeonGrid.GetLength(0); x++)
        {
            for (var y = 0; y < DungeonGrid.GetLength(1); y++)
            {
                _dungeonMap.SetTile(new Vector3Int(x, y, 0), GetTileFromInt(DungeonGrid[x, y]));
            }
        }
    }

    /// <summary>
    /// Returns a tile based on the tileIndex passed as parameter, return a wall tile by default
    /// </summary>
    /// <param name="tileID"></param>
    /// <returns></returns>
    private TileBase GetTileFromInt(int tileID)
    {
        switch (tileID)
        {
            default:
            case 0:
                return _wallTile;
            case 1:
                return _groundTile;
            case 2:
                return _roomWallTile;
        }
    }
}