﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEntityCollisionInteraction : MonoBehaviour
{
    [SerializeField]
    private EntityBase _entityToCheck;

    [SerializeField]
    private UnityEvent OnEntityEnter;
    [SerializeField]
    private UnityEvent OnEntityStay;
    [SerializeField]
    private UnityEvent OnEntityExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Unit which enters has to be the Player
        if (collision.GetComponent<EntityBase>())
        {
            OnEntityEnter.Invoke();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        // Unit which enters has to be the Player
        if (collision.GetComponent<EntityBase>())
        {
            OnEntityStay.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // Unit which enters has to be the Player
        if (collision.GetComponent<EntityBase>())
        {
            OnEntityExit.Invoke();
        }
    }
}
