﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour
{
    [SerializeField]
    private float _generalFadeDuration = 0.6f;
    private bool _isFading = false;

    public void SwitchSceneAsyncWithFade(string sceneName, bool fadeIn, float? duration = null)
    {
        StartCoroutine(FadeLevel(sceneName, fadeIn, duration));
    }

    public void FadeIn()
    {
        StartCoroutine(FadeLevel("", true));
    }

    private IEnumerator FadeLevel(string sceneName, bool fadeIn, float? duration = null)
    {
        if (!_isFading)
        {
            var time = (duration != null) ? (float)duration : _generalFadeDuration;
            var progress = 0f;
            var fadeTexture = Texture2D.blackTexture;
            fadeTexture.Resize(Screen.width, Screen.height);

            _isFading = true;

            // Fade the screen out
            while (progress < 1f)
            {
                progress += Time.deltaTime / time;
                for (int i = 0; i < fadeTexture.GetPixels().Length; i++)
                {
                    var pixel = fadeTexture.GetPixels()[i];
                    pixel.a = (fadeIn) ? 1f - progress : 0f + progress;
                }
                yield return null;
            }

            // Wait for the scene to load when fadeIn out
            if (!fadeIn)
            {
                Debug.Log("Loading...");
                yield return SceneManager.LoadSceneAsync(sceneName);
                Debug.Log("Succes!");
            }

            _isFading = false;
        }
    }
}