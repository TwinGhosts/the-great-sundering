﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EntityStatVisualizer : MonoBehaviour
{
    [SerializeField]
    protected bool _hideAtFullHealth = true;
    protected Slider _healthBar;

    private void Awake()
    {
        _healthBar = GetComponentInChildren<Slider>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (_hideAtFullHealth)
        {
            if (_healthBar.value == _healthBar.maxValue)
            {
                foreach (var graphic in GetComponentsInChildren<Graphic>())
                {
                    graphic.enabled = false;
                }
            }
            else
            {
                // TODO optimize
                foreach (var graphic in GetComponentsInChildren<Graphic>())
                {
                    graphic.enabled = true;
                }
            }
        }

        if (GetComponent<EntityBase>())
        {
            _healthBar.maxValue = GetComponent<EntityBase>().Stats.MaxHealth;
            _healthBar.value = GetComponent<EntityBase>().Stats.Health;
        }
    }
}
