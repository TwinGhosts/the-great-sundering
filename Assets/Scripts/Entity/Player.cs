﻿using UnityEngine;

public class Player : EntityBase
{
    protected override void Awake()
    {
        base.Awake();
        Util.Player = this;
    }

    private void Start()
    {
        CheckPointManager.CurrentCheckPoint = new CheckpointWrapper("On Player Start", transform.position);
    }

    protected override void Update()
    {
        base.Update();
        InputCheck();

        // Debug Rotation
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90 + Math.AngleFromPointToMouse(transform.position)));
    }

    private void InputCheck()
    {
        AbilityInputCheck();
    }

    private void AbilityInputCheck()
    {
        // Cant be casting when trying to activate another ability
        if (!Stats.IsCasting)
        {
            for (var i = 0; i < Abilities.Count; i++)
            {
                // Check for an ability key input
                if (!Abilities[i].IsOnCooldown && Input.GetKeyDown(KeyHolder.abilityKeys[i]))
                {
                    Abilities[i].Cast();
                    // Debug.Log("Ability " + i + " has been cast!");
                }
            }
        }
    }
}