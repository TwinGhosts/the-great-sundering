﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

[RequireComponent(typeof(EntityController))]
public abstract class EntityBase : MonoBehaviour
{
    [SerializeField]
    protected string _name;
    [SerializeField]
    protected string _description;

    [SerializeField]
    private List<AbilityBase> abilities = new List<AbilityBase>();

    protected List<DebuffBase> _debuffs = new List<DebuffBase>();

    [SerializeField]
    protected EntityStats _stats = new EntityStats();

    [SerializeField]
    protected UnityEvent OnNoHealth;
    protected bool _hasReachedNoHealth = false;
    protected UnitState _unitState = UnitState.Idle;

    protected virtual void Awake()
    {
        foreach (var ability in GetComponentsInChildren<AbilityBase>())
        {
            abilities.Add(ability);
        }
    }

    protected virtual void Update()
    {
        for (int i = Debuffs.Count - 1; i >= 0; i--)
        {
            var debuff = Debuffs[i];
            debuff.Update();
        }

        Stats.UpdateEffectLists();

        // Activate the death action once
        if (Stats.Health <= 0f && !_hasReachedNoHealth)
        {
            _hasReachedNoHealth = true;
            OnNoHealth.Invoke();
        }
    }

    /// <summary>
    /// Adds a debuff to the list of debuffs
    /// </summary>
    /// <param name="debuff"></param>
    public virtual void AddDebuff(DebuffBase debuff)
    {
        Debuffs.Add(debuff);
    }

    public void Remove()
    {
        Destroy(gameObject);
    }

    #region Getters & Setters
    public string Name
    {
        get
        {
            return _name;
        }
    }

    public string Description
    {
        get
        {
            return _description;
        }
    }

    public List<DebuffBase> Debuffs
    {
        get
        {
            return _debuffs;
        }
    }

    public EntityStats Stats
    {
        get
        {
            return _stats;
        }
    }

    public List<AbilityBase> Abilities
    {
        get
        {
            return abilities;
        }
    }

    public UnitState UnitState
    {
        get
        {
            return _unitState;
        }
    }
    #endregion
}
