﻿public class EnemyBase : EntityBase
{
    protected override void Awake()
    {
        base.Awake();

        OnNoHealth.AddListener(() =>
        {
            Util.QuestManager.UpdateAllQuestProgress(this);
            AddDebuff(new Debuff_RootDamage(this, 0f, 2f, 1, UnitAttackType.Divine));
        });
    }
}
