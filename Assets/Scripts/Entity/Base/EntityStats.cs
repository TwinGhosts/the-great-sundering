﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EntityStats
{
    [Header("Health")]
    [SerializeField]
    private float _health = 0f;
    [SerializeField]
    private float _maxHealth = 0f;
    // private float _refHealth = 0f;

    [Header("Resistance")]
    [SerializeField]
    private float _physicalReductionPercentage = 0f;
    [SerializeField]
    private float _magicalReductionPercentage = 0f;

    [Header("Movement")]
    [SerializeField]
    private float _movementSpeed = 0f;
    [SerializeField]
    private float _maxMovementSpeed = 0f;
    // private float _refMovementSpeed = 0f;

    [Header("Running")]
    [SerializeField]
    private float _runMultiplier = 1.4f;

    [Header("Other")]
    [SerializeField]
    private bool _isAttacking = false;
    [SerializeField]
    private bool _isCasting = false;

    private List<EffectWrap> _slowValues = new List<EffectWrap>();
    private List<EffectWrap> _speedUpValues = new List<EffectWrap>();
    private List<EffectWrap> _stunValues = new List<EffectWrap>();
    private List<EffectWrap> _rootValues = new List<EffectWrap>();
    private List<EffectWrap> _fearValues = new List<EffectWrap>();

    private List<EffectWrap> _invisibilityValues = new List<EffectWrap>();
    private List<EffectWrap> _invunerabilityValues = new List<EffectWrap>();

    private List<EffectWrap> _physicalResistanceValuesUp = new List<EffectWrap>();
    private List<EffectWrap> _physicalResistanceValuesDown = new List<EffectWrap>();

    private List<EffectWrap> _magicalResistanceValuesUp = new List<EffectWrap>();
    private List<EffectWrap> _magicalResistanceValuesDown = new List<EffectWrap>();

    public class EffectWrap
    {
        public EffectBase _base;
        public float duration;
        public float value;

        public EffectWrap(EffectBase effectBase, float duration, float value = 0f)
        {
            this.duration = duration;
            this.value = value;
            _base = effectBase;
        }
    }

    /// <summary>
    /// Removes an amount of health from this entity
    /// </summary>
    /// <param name="amount"></param>
    public virtual void Damage(float amount, UnitAttackType attackType)
    {
        if (!IsInvunerable)
        {
            // Can't damage below zero
            if (amount > 0f)
            {
                switch (attackType)
                {
                    case UnitAttackType.Physical:
                        amount -= amount * (CalculateFinalPhysicalArmor() / 100f);
                        break;
                    case UnitAttackType.Magic:
                        amount -= amount * (CalculateFinalMagicalArmor() / 100f);
                        break;
                }

                _health = (_health - amount >= 0f) ? _health - amount : 0f;
            }
        }
    }

    /// <summary>
    /// Adds an amount of health to this entity
    /// </summary>
    /// <param name="amount"></param>
    public virtual void Heal(float amount)
    {
        // Can't heal below zero
        if (amount > 0)
        {
            _health = (_health + amount <= _maxHealth) ? _health + amount : _maxHealth;
        }
    }

    /// <summary>
    /// Add a slow percentage based on the max movementSpeed to the list of slows
    /// </summary>
    /// <param name="percentage"></param>
    public virtual void Slow(Effect_Slow slow, float duration, float percentage)
    {
        _slowValues.Add(new EffectWrap(slow, duration, percentage));
    }

    /// <summary>
    /// Add a invisibility duration to the list of invisibilities
    /// </summary>
    /// <param name="percentage"></param>
    public virtual void Invisibility(Effect_Invisibility invisibility, float duration)
    {
        _invisibilityValues.Add(new EffectWrap(invisibility, duration));
    }

    /// <summary>
    /// Add a invunerability duration to the list of invunerabilities
    /// </summary>
    /// <param name="percentage"></param>
    public virtual void Invunerability(Effect_Invunerability invunerability, float duration)
    {
        _invunerabilityValues.Add(new EffectWrap(invunerability, duration));
    }

    /// <summary>
    /// Add a slow percentage based on the max movementSpeed to the list of slows
    /// </summary>
    /// <param name="percentage"></param>
    public virtual void MagicalResistanceUp(Effect_MagicArmorIncrease armorUp, float duration, float percentage)
    {
        _magicalResistanceValuesUp.Add(new EffectWrap(armorUp, duration, percentage));
    }

    /// <summary>
    /// Add a slow percentage based on the max movementSpeed to the list of slows
    /// </summary>
    /// <param name="percentage"></param>
    public virtual void MagicalResistanceDown(Effect_MagicArmorReduction armorDown, float duration, float percentage)
    {
        _magicalResistanceValuesDown.Add(new EffectWrap(armorDown, duration, percentage));
    }

    /// <summary>
    /// Add a slow percentage based on the max movementSpeed to the list of slows
    /// </summary>
    /// <param name="percentage"></param>
    public virtual void PhysicalResistanceUp(Effect_ArmorIncrease armorUp, float duration, float percentage)
    {
        _physicalResistanceValuesUp.Add(new EffectWrap(armorUp, duration, percentage));
    }

    /// <summary>
    /// Add a slow percentage based on the max movementSpeed to the list of slows
    /// </summary>
    /// <param name="percentage"></param>
    public virtual void PhysicalResistanceDown(Effect_ArmorReduction armorDown, float duration, float percentage)
    {
        _physicalResistanceValuesDown.Add(new EffectWrap(armorDown, duration, percentage));
    }

    /// <summary>
    /// Adds an instance of a root to the rootList
    /// Roots make it so that an entity can't move at all, yet unlike a stun it doesn't stop other actions
    /// And unlike a slow it doesn't change based on movement parameters
    /// </summary>
    /// <param name="duration"></param>
    public virtual void Root(Effect_Root root, float duration)
    {
        _rootValues.Add(new EffectWrap(root, duration));
    }

    /// <summary>
    /// Adds an instance of a stun to the stunList
    /// Stuns make it so that entities can't move on its own (it can by forces). Stuns also disable actions like attacking, casting etc.
    /// </summary>
    /// <param name="duration"></param>
    public virtual void Stun(Effect_Stun stun, float duration)
    {
        _stunValues.Add(new EffectWrap(stun, duration));
    }

    /// <summary>
    /// Add a speedUp percentage based on the max movementSpeed to the list of speedUps
    /// </summary>
    /// <param name="percentage"></param>
    public virtual void SpeedUp(Effect_SpeedUp speed, float duration, float percentage)
    {
        _speedUpValues.Add(new EffectWrap(speed, duration, percentage));
    }

    public virtual float CalculateFinalPhysicalArmor()
    {
        var aDown = 0f;
        foreach (var a in _physicalResistanceValuesDown)
        {
            aDown += a.value;
        }

        var aUp = _physicalReductionPercentage;
        foreach (var a in _physicalResistanceValuesUp)
        {
            aUp += a.value;
        }

        var aFinal = aUp - aDown;
        return aFinal;
    }

    public virtual float CalculateFinalMagicalArmor()
    {
        var aDown = 0f;
        foreach (var a in _magicalResistanceValuesDown)
        {
            aDown += a.value;
        }

        var aUp = _magicalReductionPercentage;
        foreach (var a in _magicalResistanceValuesUp)
        {
            aUp += a.value;
        }

        var aFinal = aUp - aDown;
        return aFinal;
    }

    /// <summary>
    /// Returns the final movementSpeed after all of the effects are added up to each other
    /// </summary>
    public virtual float CalculateFinalMovementSpeed()
    {
        var finalSpeed = _maxMovementSpeed;

        if ((IsStunned || IsRooted) || (IsStunned && IsRooted))
        {
            return 0f;
        }
        else
        {
            // Add all of the slows
            foreach (var slowVar in _slowValues)
            {
                finalSpeed -= _maxMovementSpeed * slowVar.value;
            }

            // Add all of the speedUps
            foreach (var speedUpVar in _speedUpValues)
            {
                finalSpeed += _maxMovementSpeed * speedUpVar.value;
            }
        }

        return finalSpeed;
    }

    public void UpdateEffectLists()
    {
        UpdateSingleEffectList(_slowValues);
        UpdateSingleEffectList(_stunValues);
        UpdateSingleEffectList(_rootValues);
        UpdateSingleEffectList(_speedUpValues);
        UpdateSingleEffectList(_fearValues);

        UpdateSingleEffectList(_physicalResistanceValuesUp);
        UpdateSingleEffectList(_physicalResistanceValuesDown);
        UpdateSingleEffectList(_magicalResistanceValuesUp);
        UpdateSingleEffectList(_magicalResistanceValuesDown);

        UpdateSingleEffectList(_invisibilityValues);
        UpdateSingleEffectList(_invunerabilityValues);
    }

    private void UpdateSingleEffectList(List<EffectWrap> list)
    {
        for (var i = list.Count - 1; i >= 0; i--)
        {
            list[i].duration -= Time.deltaTime;
            if (list[i].duration <= 0f)
            {
                list.Remove(list[i]);
            }
        }
    }

    #region Getters & Setters
    public bool IsCrowdControlled
    {
        get
        {
            if (IsRooted)
            {
                return true;
            }
            if (IsStunned)
            {
                return true;
            }
            return false;
        }
    }

    public bool IsInvunerable
    {
        get
        {
            return _invunerabilityValues.Count > 0;
        }
    }

    public bool IsInvisible
    {
        get
        {
            return _invisibilityValues.Count > 0;
        }
    }

    public float Health
    {
        get
        {
            return _health;
        }
    }

    public float MaxHealth
    {
        get
        {
            return _maxHealth;
        }
    }

    public float MovementSpeed
    {
        get
        {
            return _movementSpeed;
        }
    }

    public float MaxMovementSpeed
    {
        get
        {
            return _maxMovementSpeed;
        }
    }

    public float RunMultiplier
    {
        get
        {
            return _runMultiplier;
        }
    }

    public bool IsRooted
    {
        get
        {
            return _rootValues.Count > 0;
        }
    }

    public bool IsSlowed
    {
        get
        {
            return _slowValues.Count > 0;
        }
    }

    public bool IsStunned
    {
        get
        {
            return _stunValues.Count > 0;
        }
    }

    public List<EffectWrap> StunValues
    {
        get
        {
            return _stunValues;
        }
    }

    public List<EffectWrap> RootValues
    {
        get
        {
            return _rootValues;
        }
    }

    public List<EffectWrap> SlowValues
    {
        get
        {
            return _slowValues;
        }
    }

    public List<EffectWrap> SpeedUpValues
    {
        get
        {
            return _speedUpValues;
        }
    }

    public bool IsAttacking
    {
        get
        {
            return _isAttacking;
        }

        set
        {
            _isAttacking = value;
        }
    }

    public bool IsCasting
    {
        get
        {
            return _isCasting;
        }

        set
        {
            _isCasting = value;
        }
    }

    public float MagicalReductionPercentage
    {
        get
        {
            return _magicalReductionPercentage;
        }
    }

    public float PhysicalReductionPercentage
    {
        get
        {
            return _physicalReductionPercentage;
        }
    }
    #endregion Getters & Setters
}
