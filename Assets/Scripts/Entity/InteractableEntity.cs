﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractableEntity : MonoBehaviour
{
    [SerializeField]
    private float _activationRange = 2f;

    public UnityEvent OnActivation;

    public UnityEvent OnStop;

    private void Update()
    {
        if (Time.frameCount % 3 == 0 && IsInRange && Input.GetKeyDown(KeyCode.E))
        {
            OnActivation.Invoke();
            Debug.Log("Activated");
        }
    }

    public bool IsInRange
    {
        get
        {
            return (Util.Player.transform.position - transform.position).magnitude < _activationRange;
        }
    }
}
