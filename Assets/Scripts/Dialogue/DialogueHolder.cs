﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DialogueFetcher : MonoBehaviour
{
    private List<DialogueWrapper> _dialogue = new List<DialogueWrapper>();

    public string GetDialogue(string _key, int index)
    {
        foreach (var d in _dialogue)
        {
            if (d.key == _key)
            {
                index = Mathf.Clamp(index, 0, d.dialogueMessages.Count - 1);
                return d.dialogueMessages[index];
            }
        }

        return "Error > No match found!";
    }
}

[System.Serializable]
public class DialogueWrapper
{
    [SerializeField]
    public string key;
    [SerializeField]
    [TextArea]
    public List<string> dialogueMessages = new List<string>();
}