﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemObject : MonoBehaviour
{
    [SerializeField]
    private string _name;
    [SerializeField]
    [TextArea]
    private string _description;
    [Space]
    [SerializeField]
    private bool _usable = false;
    [Space]
    [SerializeField]
    private float _cooldown = 0f;

    [Header("Stacks")]
    [SerializeField]
    private int _maxStacks;
    [SerializeField]
    private int _currentStacks;
    [Space]

    private List<DebuffBase> _debuffs = new List<DebuffBase>();

    [Header("Rarity")]
    [SerializeField]
    private ItemRarity _rarity = ItemRarity.Common;

    [SerializeField]
    private Sprite _sprite;

    private float _pickUpRange = 1.4f;
    private Text _itemPickUpText;

    private string _colorStart, _colorEnd;

    private void Awake()
    {
        _itemPickUpText = GetComponentInChildren<Text>();
        SetColorStringBasedOnRarity();
        _itemPickUpText.text = _colorStart + _name + _colorEnd;
        _itemPickUpText.enabled = false;
    }

    private void Update()
    {
        PickUpCheck();
    }

    /// <summary>
    /// Check whether the player meets the criteria of picking the item up when pressing the interaction button
    /// </summary>
    private void PickUpCheck()
    {
        if ((Util.Player.transform.position - transform.position).magnitude < _pickUpRange)
        {
            _itemPickUpText.enabled = true;

            // Pick the item up
            if (Input.GetKeyDown(KeyHolder.interact) && (CanStack || (!Util.Inventory.IsFull && !CanStack)))
            {
                AddToInventory();
            }
        }
        else
        {
            _itemPickUpText.enabled = false;
        }
    }

    /// <summary>
    /// Adds this item to the player's inventory when possible, otherwise leaves it on the ground and display's inventory full message
    /// </summary>
    /// <param name="item"></param>
    public void AddToInventory()
    {
        Util.Inventory.AddItem(this, CurrentStacks);
        if (CurrentStacks <= 0)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Removes all of the current debuffs and replaces them by the parameter debuffs
    /// </summary>
    /// <param name="debuffs"></param>
    public void SetDebuffs(params DebuffBase[] debuffs)
    {
        _debuffs.Clear();
        foreach (var debuff in debuffs)
        {
            _debuffs.Add(debuff);
        }
    }

    /// <summary>
    /// Sets the start color of the item text based on the rarity
    /// </summary>
    private void SetColorStringBasedOnRarity()
    {
        _colorEnd = "</color>";
        var color = Color.white;

        switch (_rarity)
        {
            case ItemRarity.Poor:
                color = CUtil.ColorHolder.lightGray;
                break;
            default:
            case ItemRarity.Common:
                color = CUtil.ColorHolder.white;
                break;
            case ItemRarity.Uncommon:
                color = CUtil.ColorHolder.green;
                break;
            case ItemRarity.Rare:
                color = CUtil.ColorHolder.blue;
                break;
            case ItemRarity.Epic:
                color = CUtil.ColorHolder.purple;
                break;
            case ItemRarity.Legendary:
                color = CUtil.ColorHolder.orange;
                break;
        }

        _colorStart = "<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">";
    }

    #region Properties
    public string Description
    {
        get
        {
            return _description;
        }

        set
        {
            _description = value;
        }
    }

    public int CurrentStacks
    {
        get
        {
            return Mathf.Clamp(_currentStacks, 0, MaxStack);
        }
        set
        {
            _currentStacks = value;
        }
    }

    public float Cooldown
    {
        get
        {
            return Mathf.Clamp(_cooldown, 0f, 9999f);
        }
    }

    public int MaxStack
    {
        get
        {
            return Mathf.Clamp(_maxStacks, 0, 9999);
        }

        set
        {
            _maxStacks = value;
        }
    }

    public bool Usable
    {
        get
        {
            return _usable;
        }

        set
        {
            _usable = value;
        }
    }

    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }

    public List<DebuffBase> Debuffs
    {
        get
        {
            return _debuffs;
        }

        set
        {
            _debuffs = value;
        }
    }

    public bool CanStack
    {
        get
        {
            return MaxStack > 1;
        }
    }
    #endregion
}
