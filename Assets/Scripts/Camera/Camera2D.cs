using System;
using UnityEngine;

public class Camera2D : MonoBehaviour
{
    [Header("Bounds")]
    [SerializeField]
    private float _hMin = -30f;
    [SerializeField]
    private float _hMax = 30f;
    [SerializeField]
    private float _vMin = -5f;
    [SerializeField]
    private float _vMax = 15;

    [Header("Target Follow")]
    [SerializeField]
    private Transform _target;
    [SerializeField]
    private float _damping = 1;
    [SerializeField]
    private float _lookAheadFactor = 3;
    [SerializeField]
    private float _lookAheadReturnSpeed = 0.5f;
    [SerializeField]
    private float _lookAheadMoveThreshold = 0.1f;

    [Header("Zoom Properties")]
    [SerializeField]
    private float _zoomSpeed = 10f;
    [SerializeField]
    private float _zoomSensitivity = 2f;
    [SerializeField]
    private float _zoomMin = 2f;
    [SerializeField]
    private float _zoomMax = 10f;
    [SerializeField]

    private float _targetZoom = 2f;
    private Vector2 _screenShakeAmount;

    private float _offsetZ;
    private Transform _firstTarget;
    private Vector3 _lastTargetPosition;
    private Vector3 _currentVelocity;
    private Vector3 _lookAheadPos;

    // Use this for initialization
    private void Start()
    {
        if (_target)
        {
            _firstTarget = _target;
            _lastTargetPosition = _target.position;
            _offsetZ = (transform.position - _target.position).z;
            transform.parent = null;
        }
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        // Follow a target
        if (_target != null)
        {
            if (!_firstTarget)
            {
                _firstTarget = _target;
            }

            var intensity = CameraActions.Intensity / 10f;
            _screenShakeAmount = (CameraActions.IsShaking) ? new Vector2(UnityEngine.Random.Range(-intensity, intensity), UnityEngine.Random.Range(-intensity, intensity)) : Vector2.zero;

            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (_target.position - _lastTargetPosition).x;
            //float yMoveDelta = (_target.position - _lastTargetPosition).y;

            bool updateLookAheadTargetX = Mathf.Abs(xMoveDelta) > _lookAheadMoveThreshold;
            //bool updateLookAheadTargetY = Mathf.Abs(xMoveDelta) > _lookAheadMoveThreshold;

            _lookAheadPos = (updateLookAheadTargetX) ? _lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta) : Vector3.MoveTowards(_lookAheadPos, Vector3.zero, Time.deltaTime * _lookAheadReturnSpeed);

            Vector3 aheadTargetPos = _target.position + _lookAheadPos + Vector3.forward * _offsetZ + (Vector3)_screenShakeAmount;
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref _currentVelocity, _damping);

            // Update the current position and the last target position
            newPos.x = Mathf.Clamp(newPos.x, _hMin, _hMax);
            newPos.y = Mathf.Clamp(newPos.y, _vMin, _vMax);
            transform.position = newPos;
            _lastTargetPosition = _target.position;
        }

        // Makes the player able to zoom the camera
        Zoom();
        LerpZoom();
    }

    /// <summary>
    /// Zooms the camera by using the scrollwheel on the mouse.
    /// </summary>
    private void Zoom()
    {
        _targetZoom -= Input.GetAxis("Mouse ScrollWheel") * _zoomSpeed;
        _targetZoom = Mathf.Clamp(_targetZoom, _zoomMin, _zoomMax);
    }

    /// <summary>
    /// Lerps the field of view of the camera. The zooming goes faster if you have a low sensitivity. 
    /// </summary>
    private void LerpZoom()
    {
        Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, _targetZoom, (_zoomSensitivity * Time.deltaTime));
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, _zoomMin, _zoomMax);
    }

    /// <summary>
    /// Removes the camera target back to null
    /// </summary>
    public void RemoveTarget()
    {
        _target = null;
    }

    /// <summary>
    /// Sets the target the camera has to follow
    /// </summary>
    /// <param name="target">Target.</param>
    public void SetTarget(Transform target)
    {
        this._target = target;
    }

    /// <summary>
    /// Resets the target to the first acquired target
    /// </summary>
    public void ResetTarget()
    {
        if (_firstTarget)
        {
            _target = _firstTarget;
        }
        else
        {
            Debug.LogError("CAMERA > The first target does not exist!");
        }
    }

    /// <summary>
    /// Sets the orthographic zoom progress
    /// </summary>
    /// <param name="zoom">Zoom.</param>
    public void SetZoom(float zoom)
    {
        _targetZoom = Mathf.Clamp(zoom, _zoomMin, _zoomMax);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(new Vector3(_hMin, _vMin, 0f), new Vector3(_hMin, _vMax, 0f));
        Gizmos.DrawLine(new Vector3(_hMax, _vMin, 0f), new Vector3(_hMax, _vMax, 0f));
        Gizmos.DrawLine(new Vector3(_hMin, _vMax, 0f), new Vector3(_hMax, _vMax, 0f));
        Gizmos.DrawLine(new Vector3(_hMax, _vMin, 0f), new Vector3(_hMin, _vMin, 0f));
    }
}
