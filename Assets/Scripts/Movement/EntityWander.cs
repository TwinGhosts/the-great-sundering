﻿using UnityEngine;

[RequireComponent(typeof(EntityController), typeof(EntityBase))]
public class EntityWander : MonoBehaviour
{
    [SerializeField]
    private float _minWanderInterval = 1f;
    [SerializeField]
    private float _maxWanderInterval = 4f;

    private Vector2 _destination;

    private EntityBase _entityBase;
    private EntityController _entityController;
    // private EntityStats _entityStats;

    [SerializeField]
    private float _wanderRadius = 4f;
    private float _timer = 0f;
    private float _nextWanderTime = 0f;
    private Vector2 _wanderStartPosition;

    // Use this for initialization
    private void Awake()
    {
        _entityBase = GetComponent<EntityBase>();
        _entityController = GetComponent<EntityController>();
        // _entityStats = _entityBase.Stats;

        _nextWanderTime = Random.Range(_minWanderInterval, _maxWanderInterval);
        _wanderStartPosition = transform.position;
        _destination = _wanderStartPosition + Random.insideUnitCircle * _wanderRadius;
    }

    // Update is called once per frame
    private void Update()
    {
        // Has to be able
        if (_entityBase.UnitState == UnitState.Idle || _entityBase.UnitState == UnitState.Wandering)
        {
            // When the time to wander to the next pos has expired, find a new pos and reset the timer
            if (_timer > _nextWanderTime)
            {
                _timer = 0f;
                _nextWanderTime = Random.Range(_minWanderInterval, _maxWanderInterval);
                _destination = _wanderStartPosition + Random.insideUnitCircle * _wanderRadius;
                _entityController.SetTargetPosition(_destination);
            }
            else // When the time hasnt expired yet, keep counting
            {
                _timer += Time.deltaTime;
            }
        }
        else // Reset the timer when it cant wander
        {
            _timer = 0f;
        }
    }
}
