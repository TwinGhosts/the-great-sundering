﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : EntityController
{
    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
    }

    // Update is called once per frame
    protected override void LateUpdate()
    {
        CheckInput();
        base.LateUpdate();

        if (_base.Stats.IsInvisible)
        {
            var c = GetComponent<SpriteRenderer>().color;
            GetComponent<SpriteRenderer>().color = new Color(c.r, c.g, c.b, 0.5f);
        }
        else
        {
            var c = GetComponent<SpriteRenderer>().color;
            GetComponent<SpriteRenderer>().color = new Color(c.r, c.g, c.b, 1f);
        }
    }

    private void CheckInput()
    {
        var hInput = Input.GetAxis("Horizontal");
        var vInput = Input.GetAxis("Vertical");

        var hFinal = hInput * GetComponent<EntityBase>().Stats.CalculateFinalMovementSpeed() * Time.deltaTime;
        var vFinal = vInput * GetComponent<EntityBase>().Stats.CalculateFinalMovementSpeed() * Time.deltaTime;

        transform.position += (Vector3)new Vector2(hFinal, vFinal);
    }
}
