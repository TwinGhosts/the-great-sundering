﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(EntityBase))]
public class EntityController : MonoBehaviour
{
    [SerializeField]
    protected bool _useFriction = true;
    [SerializeField]
    protected float _friction = 0.9725f;

    [SerializeField]
    protected float _arriveRadius = 2f;

    protected float _lowerClampValue = 0.01f;

    protected EntityBase _base;
    protected EntityBase _target;
    protected Vector2? _targetPosition = null;

    protected Vector2 _velocity;
    protected Vector2 _force;

    protected virtual void Awake()
    {
        _base = GetComponent<EntityBase>();
    }

    // Update is called once per frame
    protected virtual void LateUpdate()
    {
        // Add the forces to the velocity and add the velocity to the position
        _velocity += _force;
        transform.position += (Vector3)_velocity;

        // Add friction to the velocity and remove the forces
        _velocity *= (_useFriction) ? _friction : 1f;
        _force = Vector2.zero;

        // Set the velocity x and y to zero when they become too low
        _velocity = new Vector2
            (
                (Mathf.Abs(_velocity.x) < _lowerClampValue) ? 0f : _velocity.x,
                (Mathf.Abs(_velocity.y) < _lowerClampValue) ? 0f : _velocity.y
            );

        if (_targetPosition != null && !_base.Stats.IsCrowdControlled)
        {
            transform.position = Vector2.MoveTowards(transform.position, (Vector2)_targetPosition, (_base.Stats.CalculateFinalMovementSpeed()) * Time.deltaTime);
            if ((transform.position - _targetPosition).Value.magnitude < 0.05f)
            {
                _targetPosition = null;
            }
        }

        if (_target && _target.Stats.IsInvisible)
        {
            _target = null;
        }

        if (_target != null && !_base.Stats.IsCrowdControlled)
        {
            transform.position = Vector2.MoveTowards(transform.position, _target.transform.position, (_base.Stats.CalculateFinalMovementSpeed()) * Time.deltaTime);
        }
    }

    /// <summary>
    /// Move towards an angle
    /// </summary>
    /// <param name="angle"></param>
    public void MoveDirection(float angle)
    {
        _velocity = Math.VectorFromAngle(angle).normalized * _base.Stats.CalculateFinalMovementSpeed() * Time.deltaTime;
    }

    /// <summary>
    /// Move towards a X and Y direction from the current position
    /// </summary>
    /// <param name="direction"></param>
    public void MoveDirection(Vector2 direction)
    {
        _velocity = direction.normalized * _base.Stats.CalculateFinalMovementSpeed() * Time.deltaTime;
    }

    /// <summary>
    /// Chase a target by running right towards it
    /// </summary>
    /// <param name="target"></param>
    public void MoveChase(EntityBase target)
    {
        _target = target;
    }

    /// <summary>
    /// Move towards a target while moving around obstacles
    /// </summary>
    public void MoveSmart()
    {
        // TODO
    }

    public void SetTargetPosition(Vector2 targetPosition)
    {
        _targetPosition = targetPosition;
    }

    /// <summary>
    /// Knocks this entity back by a force from a position with power
    /// *NOTE that if the distance from the impactPosition increases, the power has to increase to have an effect
    /// </summary>
    /// <param name="impactPosition"></param>
    /// <param name="power"></param>
    public void Knockback(Vector2 impactPosition, float power)
    {
        if (!_base.Stats.IsRooted)
        {
            var direction = ((Vector2)transform.position - impactPosition);
            _force += direction.normalized * power * Time.deltaTime;
        }
    }

    public void ResetTarget()
    {
        _target = null;
    }

    public Vector2? TargetPosition
    {
        get
        {
            return _targetPosition;
        }
    }
}
