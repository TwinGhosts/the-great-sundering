﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public ColorHolder ColorHolder;
    public static GameState GameState = GameState.Playing;

    // Use this for initialization
    private void Awake()
    {
        Util.GameController = this;
    }

    // Update is called once per frame
    private void Update()
    {

    }
}
