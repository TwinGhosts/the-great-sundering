﻿using UnityEngine;

public abstract class QuestObjective : MonoBehaviour
{
    public abstract bool IsCompleted
    {
        get;
    }

    public abstract void UpdateProgress();
    public abstract void UpdateProgress(EntityBase entity);
    public abstract void UpdateProgress(ItemObject item);
}
