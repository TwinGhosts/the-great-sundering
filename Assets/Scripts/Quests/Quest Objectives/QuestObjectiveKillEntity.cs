﻿using UnityEngine;

public class QuestObjectiveEscort : QuestObjective
{
    private EntityBase _escortTarget;
    private Vector2[] _waypoints;
    private int _currentWaypointIndex = 0;
    private string _locationName = "";
    private float _locationRadius = 1f;

    public QuestObjectiveEscort(EntityBase escortTarget, Vector2[] waypoints, float locationRadius, string locationName)
    {
        _escortTarget = escortTarget;
        _waypoints = waypoints;
        _locationName = locationName;
        _locationRadius = locationRadius;
    }

    public override bool IsCompleted
    {
        get
        {
            var waypointCheck = _currentWaypointIndex >= _waypoints.Length;
            var entityCheck = _escortTarget && _escortTarget.Stats.Health > 0f;
            return waypointCheck && entityCheck;
        }
    }

    public override string ToString()
    {
        return "Escorted " + _escortTarget.Name + " to " + _locationName;
    }

    public override void UpdateProgress(EntityBase entity)
    {
        if (_escortTarget == entity && InRangeOfNextWaypoint)
        {
            _currentWaypointIndex++;
            Debug.Log(ToString());
        }
    }

    private bool InRangeOfNextWaypoint
    {
        get
        {
            return (_currentWaypointIndex + 1 < _waypoints.Length && ((Vector2)_escortTarget.transform.position - _waypoints[_currentWaypointIndex + 1]).magnitude < _locationRadius);
        }
    }

    public override void UpdateProgress()
    {
    }

    public override void UpdateProgress(ItemObject item)
    {
    }
}
