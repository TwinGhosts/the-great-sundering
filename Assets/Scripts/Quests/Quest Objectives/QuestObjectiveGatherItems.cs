﻿using UnityEngine;

public class QuestObjectiveGatherItems : QuestObjective
{
    [SerializeField]
    private ItemObject _item;
    [SerializeField]
    private int _amount;
    private int _gatheredAmount = 0;

    public override bool IsCompleted
    {
        get
        {
            var itemCountCheck = Util.Inventory.ContainsItemAmount(_item, _amount);
            return itemCountCheck;
        }
    }

    public override string ToString()
    {
        return "Obtained " + _amount + "/" + _gatheredAmount + _item.Name + ((_amount > 1) ? "s" : "");
    }

    public override void UpdateProgress(EntityBase entity)
    {
    }

    public override void UpdateProgress(ItemObject item)
    {
    }

    public override void UpdateProgress()
    {
    }
}
