﻿using UnityEngine;

public class QuestObjectiveKillEntity : QuestObjective
{
    private string _entityName;
    private int _killCount;

    private int _currentKillCount = 0;

    public QuestObjectiveKillEntity(string entityName, int killCount)
    {
        _entityName = entityName;
        _killCount = killCount;
    }

    public override bool IsCompleted
    {
        get
        {
            return _currentKillCount >= _killCount;
        }
    }

    public override string ToString()
    {
        return "Killed: " + Mathf.Clamp(_currentKillCount, 0, _killCount) + " / " + _killCount;
    }

    public override void UpdateProgress(EntityBase entity)
    {
        if (entity.Name.Equals(_entityName))
        {
            _currentKillCount++;
            Debug.Log(ToString());
        }
    }

    public override void UpdateProgress()
    {
    }

    public override void UpdateProgress(ItemObject item)
    {
    }
}
