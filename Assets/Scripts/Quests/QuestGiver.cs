﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiver : MonoBehaviour
{
    [SerializeField]
    private List<Quest> _quests;

    [SerializeField]
    private int _questProgress;

    public List<Quest> Quests
    {
        get
        {
            return _quests;
        }
    }

    public void GiveNextQuest()
    {
        Util.QuestManager.SetQuestToActive(Quests[_questProgress]);
    }

    public int QuestProgress
    {
        get
        {
            return _questProgress;
        }

        set
        {
            _questProgress = value;
        }
    }
}
