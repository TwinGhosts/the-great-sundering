﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    private List<Quest> _activeQuests = new List<Quest>();
    private List<Quest> _completedQuests = new List<Quest>();

    public List<Quest> ActiveQuests
    {
        get
        {
            return _activeQuests;
        }
    }
    public List<Quest> CompletedQuests
    {
        get
        {
            return _completedQuests;
        }
    }

    private void Awake()
    {
        Util.QuestManager = this;
    }

    /// <summary>
    /// Add a quest to the active quest list when it hasn't been active yet
    /// </summary>
    /// <param name="quest"></param>
    public void SetQuestToActive(Quest quest)
    {
        if (!ActiveQuests.Contains(quest))
        {
            _activeQuests.Add(quest);
        }
        else
        {
            Debug.Log("ERROR QUEST > Quest already active!");
        }
    }

    /// <summary>
    /// Updates all of the active quests' progress to see if they have advanced in progression or are completed
    /// </summary>
    /// <param name="entity"></param>
    public void UpdateAllQuestProgress(EntityBase entity)
    {
        foreach (var quest in ActiveQuests)
        {
            foreach (var questObjective in quest.Objectives)
            {
                if (questObjective is QuestObjectiveKillEntity)
                {
                    questObjective.UpdateProgress(entity);
                }
            }
        }
    }
}
