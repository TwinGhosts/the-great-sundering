﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Quest
{
    [Header("Basics")]
    [SerializeField]
    private string _name;
    [SerializeField]
    private string _description;
    [SerializeField]
    private bool _optional = true;
    [SerializeField]
    private QuestObjectiveOrder _order = QuestObjectiveOrder.Single;
    [SerializeField]
    private QuestReward _reward;

    [Header("Objectives")]
    [SerializeField]
    private List<QuestObjective> _objectives = new List<QuestObjective>();

    [Header("Turn in")]
    [SerializeField]
    private InteractableEntity _questGiver;
    [SerializeField]
    private InteractableEntity _questEnder;

    public bool Optional
    {
        get
        {
            return _optional;
        }
    }

    public string Name
    {
        get
        {
            return _name;
        }
    }

    public string Description
    {
        get
        {
            return _description;
        }
    }

    public QuestObjectiveOrder Order
    {
        get
        {
            return _order;
        }
    }

    public List<QuestObjective> Objectives
    {
        get
        {
            return _objectives;
        }
    }

    public QuestReward Reward
    {
        get
        {
            return _reward;
        }
    }

    public bool IsCompleted
    {
        get
        {
            var tempCheck = true;
            foreach (var objective in Objectives)
            {
                if (!objective.IsCompleted)
                {
                    tempCheck = false;
                }
            }
            return tempCheck;
        }
    }

    public Quest(string questName, string questDescription, params QuestObjective[] questObjectives)
    {
        _name = questName;
        _description = questDescription;

        foreach (var objective in questObjectives)
        {
            Objectives.Add(objective);
        }
    }

    public string GetObjectivesAsString()
    {
        var tempString = "";
        foreach (var objective in Objectives)
        {
            tempString += objective.ToString();
        }
        return tempString;
    }

    public void Accept()
    {
        Util.QuestManager.SetQuestToActive(this);
    }
}
