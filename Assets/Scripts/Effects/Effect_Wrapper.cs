﻿public class EffectWrapper<E, F>
{
    public EffectWrapper(E element, F value)
    {
        Element = element;
        Value = value;
    }

    public void Update(F newValue)
    {
        Value = newValue;
    }

    public F Value { get; private set; }
    public E Element { get; private set; }
}