﻿using System.Collections.Generic;

/// <summary>
/// An effect which deals damage on activation to a target or a list of targets
/// </summary>
public class Effect_Damage : EffectBase
{
    private float _damage;
    private UnitAttackType _aType;

    public Effect_Damage(float damage, UnitAttackType aType)
    {
        _damage = damage;
        _aType = aType;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.Damage(_damage, _aType);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.Damage(_damage, _aType);
        }
    }
}