﻿using System.Collections.Generic;

/// <summary>
/// An effect which speeds a target up for an amount of time
/// </summary>
public class Effect_SpeedUp : EffectBase
{
    private float _speedUp;

    public Effect_SpeedUp(float duration, float speedUp)
    {
        _speedUp = speedUp;
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.SpeedUp(this, _duration, _speedUp);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.SpeedUp(this, _duration, _speedUp);
        }
    }
}