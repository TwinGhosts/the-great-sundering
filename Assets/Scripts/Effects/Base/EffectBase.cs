﻿using System.Collections.Generic;

public abstract class EffectBase
{
    protected float _duration;

    public abstract void Activate(EntityBase target);
    public abstract void Activate(List<EntityBase> targets);
}
