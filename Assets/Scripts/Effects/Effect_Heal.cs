﻿using System.Collections.Generic;

/// <summary>
/// An effect which heals a target or targets for an amount
/// </summary>
public class Effect_Heal : EffectBase
{
    private float _heal;

    public Effect_Heal(float heal)
    {
        _heal = heal;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.Heal(_heal);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.Heal(_heal);
        }
    }
}