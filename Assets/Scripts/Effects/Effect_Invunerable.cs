﻿using System.Collections.Generic;

/// <summary>
/// An effect which slows a target for an amount of time
/// </summary>
public class Effect_Invunerability : EffectBase
{
    public Effect_Invunerability(float duration)
    {
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.Invunerability(this, _duration);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.Invunerability(this, _duration);
        }
    }
}