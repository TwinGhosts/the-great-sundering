﻿using System.Collections.Generic;

/// <summary>
/// An effect which renders the target unable to move and act for an amount of time
/// </summary>
public class Effect_Stun : EffectBase
{
    public Effect_Stun(float duration)
    {
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.Stun(this, _duration);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.Stun(this, _duration);
        }
    }
}
