﻿using System.Collections.Generic;

/// <summary>
/// An effect which slows a target for an amount of time
/// </summary>
public class Effect_ArmorReduction : EffectBase
{
    private float _armorReduction;

    public Effect_ArmorReduction(float slow, float duration)
    {
        _armorReduction = slow;
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.PhysicalResistanceDown(this, _duration, _armorReduction);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.PhysicalResistanceDown(this, _duration, _armorReduction);
        }
    }
}

/// <summary>
/// An effect which slows a target for an amount of time
/// </summary>
public class Effect_ArmorIncrease : EffectBase
{
    private float _armorIncrease;

    public Effect_ArmorIncrease(float slow, float duration)
    {
        _armorIncrease = slow;
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.PhysicalResistanceUp(this, _duration, _armorIncrease);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.PhysicalResistanceUp(this, _duration, _armorIncrease);
        }
    }
}

/// <summary>
/// An effect which slows a target for an amount of time
/// </summary>
public class Effect_MagicArmorReduction : EffectBase
{
    private float _armorReduction;

    public Effect_MagicArmorReduction(float slow, float duration)
    {
        _armorReduction = slow;
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.MagicalResistanceDown(this, _duration, _armorReduction);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.MagicalResistanceDown(this, _duration, _armorReduction);
        }
    }
}

/// <summary>
/// An effect which slows a target for an amount of time
/// </summary>
public class Effect_MagicArmorIncrease : EffectBase
{
    private float _armorIncrease;

    public Effect_MagicArmorIncrease(float slow, float duration)
    {
        _armorIncrease = slow;
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.MagicalResistanceUp(this, _duration, _armorIncrease);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.MagicalResistanceUp(this, _duration, _armorIncrease);
        }
    }
}