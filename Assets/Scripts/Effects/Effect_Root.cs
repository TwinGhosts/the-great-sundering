﻿using System.Collections.Generic;

/// <summary>
/// An effect which slows a target for 100% for an amount of time. This effect cant be stopped with SpeedUp effects
/// </summary>
public class Effect_Root : EffectBase
{
    public Effect_Root(float duration)
    {
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.Root(this, _duration);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.Root(this, _duration);
        }
    }
}
