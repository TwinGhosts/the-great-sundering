﻿using System.Collections.Generic;

/// <summary>
/// An effect which slows a target for an amount of time
/// </summary>
public class Effect_Slow : EffectBase
{
    private float _slow;

    public Effect_Slow(float slow, float duration)
    {
        _slow = slow;
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.Slow(this, _duration, _slow);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.Slow(this, _duration, _slow);
        }
    }
}