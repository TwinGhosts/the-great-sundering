﻿using System.Collections.Generic;

/// <summary>
/// An effect which slows a target for an amount of time
/// </summary>
public class Effect_Invisibility : EffectBase
{
    public Effect_Invisibility(float duration)
    {
        _duration = duration;
    }

    public override void Activate(EntityBase target)
    {
        target.Stats.Invisibility(this, _duration);
    }

    public override void Activate(List<EntityBase> targets)
    {
        foreach (var target in targets)
        {
            target.Stats.Invisibility(this, _duration);
        }
    }
}