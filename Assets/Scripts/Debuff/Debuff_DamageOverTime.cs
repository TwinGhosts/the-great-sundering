﻿public class Debuff_DamageOverTime : DebuffBase
{
    public Debuff_DamageOverTime(EntityBase target, float damage, float duration, int intervals, UnitAttackType aType) : base(target, duration, intervals)
    {
        Init(target, duration, intervals);
        _onIntervalEffects.Add(new Effect_Damage(damage / intervals, aType));
    }
}