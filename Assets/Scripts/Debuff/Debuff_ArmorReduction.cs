﻿using UnityEngine;

public class Debuff_ArmorReduction : DebuffBase
{
    public Debuff_ArmorReduction(EntityBase target, float armorReductionPercentage, float duration, int intervals, UnitAttackType aType) : base(target, duration, intervals)
    {
        Init(target, duration, intervals);

        switch (aType)
        {
            case UnitAttackType.Physical:
                _onInstantEffects.Add(new Effect_ArmorReduction(armorReductionPercentage, duration));
                break;
            case UnitAttackType.Magic:
                _onInstantEffects.Add(new Effect_MagicArmorReduction(armorReductionPercentage, duration));
                break;
            case UnitAttackType.Divine:
                // Cant resist divine!
                break;
        }

        OnInstantActivate();
    }
}