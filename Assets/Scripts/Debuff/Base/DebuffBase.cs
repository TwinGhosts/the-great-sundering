﻿using System.Collections.Generic;
using UnityEngine;

public abstract class DebuffBase
{
    [Header("Basics")]
    protected string _name = "Debuff";
    protected string _description = "Description (debuff)";

    protected string Name
    {
        get { return _name; }
    }
    protected string Description
    {
        get { return _description; }
    }

    protected EntityBase _target;

    [SerializeField]
    protected float _duration = 0f;

    [SerializeField]
    protected int _intervals = 0;
    protected int _currentInterval = 0;
    protected float _intervalTime = 0f;

    [SerializeField]
    protected float _travelDistance = 10f;

    protected float _progress = 0f;

    [Header("Stackable")]
    [SerializeField]
    protected bool _canStack = false;

    [SerializeField]
    protected int _stacks = 0;

    [Header("Area of Effect")]
    [SerializeField]
    protected bool _hasAoE = false;
    [SerializeField]
    protected float _AoERadius;

    protected bool _firstTick = true;

    [Header("Effect Lists")]
    [SerializeField]
    protected List<EffectBase> _onInstantEffects = new List<EffectBase>();
    [SerializeField]
    protected List<EffectBase> _onHitEffects = new List<EffectBase>();
    [SerializeField]
    protected List<EffectBase> _onIntervalEffects = new List<EffectBase>();
    [SerializeField]
    protected List<EffectBase> _onDistanceReachedEffects = new List<EffectBase>();
    [SerializeField]
    protected List<EffectBase> _onExpireEffects = new List<EffectBase>();

    public DebuffBase(EntityBase target, float duration, int intervals)
    {
        Init(target, duration, intervals);
    }

    /// <summary>
    /// Initialise this debuff
    /// </summary>
    /// <param name="target"></param>
    /// <param name="duration"></param>
    /// <param name="intervals"></param>
    protected void Init(EntityBase target, float duration, int intervals)
    {
        _target = target;
        _duration = duration;
        _intervals = intervals;

        if (_intervals > 0)
        {
            _intervalTime = _duration / _intervals;
        }
    }

    /// <summary>
    /// Updates this debuff, needs to be called in a monobehaviour Update
    /// </summary>
    public virtual void Update()
    {
        if (_firstTick)
        {
            _firstTick = false;
            OnInstantActivate();
        }

        // Increment the time it has been active
        _progress += Time.deltaTime;

        // Check whether an interval time has passed, activate the interval method when it happened
        if (_progress >= _currentInterval * _intervalTime && _currentInterval < _intervals)
        {
            OnIntervalActivate();
            _currentInterval++;
        }

        // Delete this debuff when its duration has expired
        if (_progress > _duration)
        {
            if (_target.Debuffs.Contains(this))
            {
                OnExpireActivate();
                _target.Debuffs.Remove(this);
            }
        }
    }

    /// <summary>
    /// Return all of the Entities within a radius of the afflicted target
    /// </summary>
    /// <param name="radius"></param>
    /// <returns></returns>
    public List<EntityBase> GetAoETargets(float radius)
    {
        List<EntityBase> _tempList = new List<EntityBase>();
        foreach (var entity in Physics2D.OverlapCircleAll(_target.transform.position, radius))
        {
            if (entity.GetComponent<EntityBase>())
            {
                _tempList.Add(entity.GetComponent<EntityBase>());
            }
        }

        return _tempList;
    }

    /// <summary>
    /// Refreshes the duration of the debuff back to the starting duration
    /// </summary>
    public void RefreshDuration()
    {
        _progress = 0f;
        _currentInterval = 0;

        if (_intervals > 0)
        {
            _intervalTime = _duration / _intervals;
        }
    }

    /// <summary>
    /// Event which triggers when hitting something that can be hit by the ability
    /// </summary>
    public virtual void OnHitActivate()
    {
        foreach (var effect in _onHitEffects)
        {
            effect.Activate(_target);
        }

        // Debug.Log("OnHit activation!");
    }

    /// <summary>
    /// Event which triggers when reaching a set amount of distance after moving. Works for projectiles
    /// </summary>
    public virtual void OnDistanceReachedActivate()
    {
        foreach (var effect in _onDistanceReachedEffects)
        {
            effect.Activate(_target);
        }

        // Debug.Log("DistanceReached activation!");
    }

    /// <summary>
    /// Event which triggers at every calculate interval of the debuff.
    /// </summary>
    public virtual void OnIntervalActivate()
    {
        foreach (var effect in _onIntervalEffects)
        {
            effect.Activate(_target);
        }

        // Debug.Log("Interval activation!");
    }

    /// <summary>
    /// Event which triggers instantly when the ability is cast
    /// </summary>
    public virtual void OnInstantActivate()
    {
        foreach (var effect in _onInstantEffects)
        {
            effect.Activate(_target);
        }

        // Debug.Log("Instant activation!");
    }

    /// <summary>
    /// Event which triggers when the duration of the debuff has expired
    /// </summary>
    public virtual void OnExpireActivate()
    {
        foreach (var effect in _onExpireEffects)
        {
            effect.Activate(_target);
        }

        // Debug.Log("Expired activation!");
    }
}
