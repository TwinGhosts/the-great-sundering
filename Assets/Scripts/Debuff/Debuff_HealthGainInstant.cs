﻿public class Debuff_HealthGainInstant : DebuffBase
{
    public Debuff_HealthGainInstant(EntityBase target, float healthGain) : base(target, 0f, 1)
    {
        Init(target, 0f, 1);
        _onInstantEffects.Add(new Effect_Heal(healthGain));
    }
}

public class Debuff_HealthGainOverTime : DebuffBase
{
    public Debuff_HealthGainOverTime(EntityBase target, float healthGain, float duration, int intervals) : base(target, duration, intervals)
    {
        Init(target, duration, intervals);
        _onIntervalEffects.Add(new Effect_Heal(healthGain / intervals));
    }
}
