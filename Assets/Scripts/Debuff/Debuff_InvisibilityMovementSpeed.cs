﻿public class Debuff_InvisibilityMovementSpeed : DebuffBase
{
    public Debuff_InvisibilityMovementSpeed(EntityBase target, float duration, float speedUp) : base(target, duration, 0)
    {
        Init(target, duration, 0);
        _onInstantEffects.Add(new Effect_Invisibility(duration));
        _onInstantEffects.Add(new Effect_SpeedUp(duration, speedUp));
    }
}