﻿using UnityEngine;

public class Debuff_StunDamage : DebuffBase
{
    public Debuff_StunDamage(EntityBase target, float damage, float duration, int intervals, UnitAttackType aType) : base(target, duration, intervals)
    {
        Init(target, duration, intervals);

        switch (aType)
        {
            case UnitAttackType.Physical:
                _onIntervalEffects.Add(new Effect_Damage(damage, UnitAttackType.Physical));
                break;
            case UnitAttackType.Magic:
                _onIntervalEffects.Add(new Effect_Damage(damage, UnitAttackType.Magic));
                break;
            case UnitAttackType.Divine:
                _onIntervalEffects.Add(new Effect_Damage(damage, UnitAttackType.Divine));
                break;
        }

        _onIntervalEffects.Add(new Effect_Stun(_duration));
    }
}