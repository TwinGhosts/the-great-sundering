﻿public class Debuff_Invunerability : DebuffBase
{
    public Debuff_Invunerability(EntityBase target, float duration, float speedUp) : base(target, duration, 0)
    {
        Init(target, duration, 0);
        _onInstantEffects.Add(new Effect_Invunerability(duration));
        _onInstantEffects.Add(new Effect_SpeedUp(duration, speedUp));
    }
}