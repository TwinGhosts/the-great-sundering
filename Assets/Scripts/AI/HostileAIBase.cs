﻿using MEC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(EntityController), typeof(EntityStats))]
public class HostileAIBase : MonoBehaviour
{
    [Header("Aggro")]
    [SerializeField]
    protected float _aggroRange = 4f;
    [SerializeField]
    protected float _callForHelpRange = 3f;
    [SerializeField]
    protected float _stopChaseRange = 6f;
    [SerializeField]
    protected AIState state = AIState.IDLE;

    public UnityEvent OnAggro;
    public UnityEvent OnAggroLoss;
    public UnityEvent OnAttack;

    protected EntityBase _target = null;
    protected List<AbilityBase> _abilities = new List<AbilityBase>();

    protected Vector2 _aggroStartPosition;
    protected float _aggroResetTimer = 1.5f;

    protected AIState State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
        }
    }
    protected AIState _previousState;

    protected virtual void Update()
    {
        if (Time.frameCount % 10 == 0)
        {
            if (GetComponent<EntityWander>())
            {
                GetComponent<EntityWander>().enabled = (State & AIState.IDLE) != 0;
            }

            // TODO Change to non-player as well
            if ((State & AIState.IDLE) != 0)
            {
                if ((Util.Player.transform.position - transform.position).magnitude < _aggroRange)
                {
                    AttackTarget(Util.Player);
                }
            }

            // TODO Change to non-player as well
            if ((State & AIState.ATTACKING) != 0 && (Util.Player.transform.position - transform.position).magnitude > _stopChaseRange)
            {
                LoseTarget();
            }

            if (_target && _target.Stats.IsInvisible)
            {
                LoseTarget();
            }
        }
    }

    /// <summary>
    /// Lets the AI chase a target
    /// </summary>
    /// <param name="target"></param>
    public virtual void AttackTarget(EntityBase target)
    {
        _target = target;
        _previousState = State;
        _aggroStartPosition = transform.position;
        State = AIState.ATTACKING;
        GetComponent<EntityController>().MoveChase(target);
    }

    /// <summary>
    /// Lets the AI call to other AI's in range for aid
    /// </summary>
    /// <param name="radius"></param>
    public virtual void CallForHelp()
    {
        var entities = Physics2D.OverlapCircleAll(transform.position, _callForHelpRange);
        foreach (var entity in entities)
        {
            if (entity.GetComponent<HostileAIBase>() && (entity.GetComponent<HostileAIBase>().State & (AIState.IDLE | AIState.MOVING)) != 0 && entity.GetComponent<EnemyBase>())
            {
                entity.GetComponent<HostileAIBase>().CallForHelp();
            }
        }
    }

    /// <summary>
    /// Makes the AI reset the target and resume what it was doing previously
    /// </summary>
    public virtual void LoseTarget()
    {
        if (_target)
        {
            _target = null;
        }

        StartCoroutine(ResetPosition());
    }

    /// <summary>
    /// Handles the AI state of returning to its original position before doing what it did
    /// </summary>
    /// <returns></returns>
    protected IEnumerator ResetPosition()
    {
        State = AIState.RESETTING;
        GetComponent<EntityController>().ResetTarget();
        yield return new WaitForSeconds(_aggroResetTimer);
        yield return ReturnToPosition();
        State = _previousState;
    }

    /// <summary>
    /// Returns the AI to its starting position (or aggro starting position)
    /// </summary>
    /// <returns></returns>
    protected IEnumerator ReturnToPosition()
    {
        // Attempt to return to the starting position
        GetComponent<EntityController>().SetTargetPosition(_aggroStartPosition);

        // Check whether the AI has returned to the starting position
        while ((_aggroStartPosition - (Vector2)transform.position).magnitude > 0.15f)
        {
            yield return null;
        }
    }
}

[Flags]
public enum AIState
{
    NONE = 0,

    IDLE = 1,
    MOVING = 2,
    ATTACKING = 4,
    SUPPORTING = 8,
    FLEEING = 16,
    RESETTING = 32,
}