﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField]
    private InventorySlot[] _slots = new InventorySlot[25];

    private void Awake()
    {
        Util.Inventory = this;

        for (int i = 0; i < _slots.Length; i++)
        {
            _slots[i] = new InventorySlot(i, null);
        }
    }

    public void AddItem(ItemObject item)
    {
        FirstEmptySlot(item).SetItem(item, 1);
        item.CurrentStacks = 0;
    }

    public void AddItem(ItemObject item, int amount)
    {
        FirstEmptySlot(item).SetItem(item, amount);
        item.CurrentStacks -= amount;
    }

    /// <summary>
    /// Removes an item without checking stacks from the inventory
    /// </summary>
    /// <param name="item"></param>
    public void RemoveItem(ItemObject item)
    {
        if (!ContainsItem(item))
        {
            GetSlotWithItem(item).RemoveItem();
        }
    }

    /// <summary>
    /// Removes a number of stacks from the given item. Removes the item when the stacks reach 0
    /// </summary>
    /// <param name="item"></param>
    /// <param name="amount"></param>
    public void RemoveItem(ItemObject item, int amount)
    {
        if (!ContainsItem(item))
        {
            GetSlotWithItem(item).RemoveItemStacks(amount);
        }
    }

    /// <summary>
    /// Returns the first empty inventoryslot
    /// </summary>
    /// <param name="itemObject"></param>
    /// <returns></returns>
    public InventorySlot FirstEmptySlot(ItemObject itemObject)
    {
        if (ContainsItem(itemObject))
        {
            return GetSlotWithItem(itemObject);
        }
        else
        {
            foreach (var slot in _slots)
            {
                if (!slot.IsOccupied)
                {
                    return slot;
                }
            }
        }

        Debug.LogWarning("No empty slot found!");
        return null;
    }

    /// <summary>
    /// Returns the first found slot with the given item. Returns null when no item can be found
    /// </summary>
    /// <param name="itemObject"></param>
    /// <returns></returns>
    private InventorySlot GetSlotWithItem(ItemObject itemObject)
    {
        foreach (var slot in _slots)
        {
            if (slot.SlotObject == itemObject)
            {
                return slot;
            }
        }

        // No ItemObject found
        return null;
    }

    /// <summary>
    /// Returns whether the inventory contains at least one item of the given type
    /// </summary>
    /// <param name="itemObject"></param>
    /// <returns></returns>
    public bool ContainsItem(ItemObject itemObject)
    {
        foreach (var slot in _slots)
        {
            if (slot.SlotObject == itemObject)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Checks whether the given item has an amount of stacks in the inventory
    /// </summary>
    /// <param name="itemObject"></param>
    /// <param name="stacks"></param>
    /// <returns></returns>
    public bool ContainsItemAmount(ItemObject itemObject, int stacks)
    {
        var count = 0;
        foreach (var slot in _slots)
        {
            if (slot.SlotObject == itemObject)
            {
                count++;
            }
        }
        return count == stacks;
    }

    /// <summary>
    /// Returns whether all of the inventorySlots are occupied
    /// </summary>
    public bool IsFull
    {
        get
        {
            foreach (var slot in _slots)
            {
                if (!slot.IsOccupied)
                {
                    return false;
                }
            }
            return true;
        }
    }
}

/// <summary>
/// Data holder for a single inventorySlot
/// </summary>
public class InventorySlot
{
    private int _index = 0;
    private int _maxStacks = 1;
    private int _currentStacks = 1;

    public InventorySlot(int index, ItemObject slotObject)
    {
        _index = index;
        SlotObject = slotObject;
    }

    /// <summary>
    /// Set the item of this InventorySlot
    /// </summary>
    /// <param name="item"></param>
    /// <param name="amount"></param>
    public void SetItem(ItemObject item, int amount)
    {
        // TODO | Move itemObject to visual inventory

        if ((!IsOccupied && item.CanStack) || (IsOccupied && item == SlotObject && item.CanStack))
        {
            if (!IsOccupied)
            {
                SlotObject = item;
            }

            if (_currentStacks + amount <= _maxStacks)
            {
                _currentStacks += amount;
            }
            else
            {
                // TODO | Remove stacks from itemObject when not fully picked up
                _currentStacks = _maxStacks;
            }
        }
        else if (IsOccupied && item != SlotObject && !item.CanStack)
        {
            // Can't pick item up
        }
    }

    /// <summary>
    /// Completely remove item from slot
    /// </summary>
    public void RemoveItem()
    {
        SlotObject = null;
    }

    /// <summary>
    /// Removes stacks from the item
    /// </summary>
    /// <param name="amount"></param>
    public void RemoveItemStacks(int amount)
    {
        SlotObject.CurrentStacks = SlotObject.CurrentStacks - amount;
    }

    #region Properties
    public ItemObject SlotObject { get; private set; }
    public int Index { get { return _index; } }
    public int CurrentStacks { get { return _currentStacks; } }
    public int MaxStacks { get { return _maxStacks; } }
    public bool IsOccupied { get { return SlotObject != null; } }
    #endregion Properties
}
